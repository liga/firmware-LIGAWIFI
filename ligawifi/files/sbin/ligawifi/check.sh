#!/bin/sh
# Copyright © 2011-2013 LIGA WiFi: Communities, Lda
# All rights reserved.

# Load in the settings
. /sbin/ligawifi/settings.sh

echo "LIGA WiFi Connection Checker"
echo "----------------------------------------------------------------"

# Deletes any bad mesh paths that may occur from time to time
iw ${if_mesh} mpath dump | grep '00:00:00:00:00:00' | while read line; do
	iw ${if_mesh} mpath del $(echo $line | awk '{ print $1 }')
done

# Tests LAN Connectivity
if [ "$(ping -c 2 ${ip_gateway})" ]; then
	lan_status=1
else
	lan_status=0
fi

# Tests WAN Connectivity
if [ "$(ping -c 2 www.google.com)" ]; then
	wan_status=1
else
	wan_status=0
fi

# Tests DNS Connectivity
if [ "$(nslookup www.google.com)" ]; then
	dns_status=1
else
	dns_status=0
fi

# Log that result
log_message "check: LAN: ${lan_status} | WAN: ${wan_status} | DNS: ${dns_status}"
